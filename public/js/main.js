/**
 * Created by Emin Guliyev on 28/05/2014.
 */
$(document).ready(function () {

    $(".user-type-menu li a").click(function () {
        var current = $(this);
        var enclosingDiv = current.parents(".input-group-btn");
        var btn = enclosingDiv.find(".btn span:first-child");
        btn.text(current.text());
        enclosingDiv.find("input[name='searchParams.userTypeForSearch']").val(current.text());
    });

    $(".setUserTypeOnclickAndSubmit").bind("click", function (evt) {
        var selectedValue = $(this).find("span.user-type").text();
        var userTypeForSearch = $("#searchFullForm input[name='searchParams.userTypeForSearch']");
        userTypeForSearch.val(selectedValue);
        $("#searchFullForm input[name='searchParams.pageForSearch']").val(1);

        $("#searchFullForm").submit();
        return false;
    });

    $(".setSubjectOnclickAndSubmit").bind("click", function (evt) {
        var selectedValue = $(this).find("input[type=hidden]").val();
        var subjectIdForSearch = $("#searchFullForm input[name='searchParams.subjectIdForSearch']");
        subjectIdForSearch.val(selectedValue);
        $("#searchFullForm input[name='searchParams.pageForSearch']").val(1);

        $("#searchFullForm").submit();
        return false;
    });

    $(".setPageOnclickAndSubmit").bind("click", function (evt) {
        var classOfEnclosingLi = $(this).parents("li").attr('class');
        if (classOfEnclosingLi == "active") {
            return false;
        }
        var selectedValue = $(this).text();
        var pageForSearch = $("#searchFullForm input[name='searchParams.pageForSearch']");
        pageForSearch.val(selectedValue);

        $("#searchFullForm").submit();
        return false;
    });

    $(".minMaxPaging").bind("click", function (evt) {
        var classOfEnclosingLi = $(this).parents("li").attr('class');
        if (classOfEnclosingLi == "disabled") {
            return false;
        }
        var selectedValue = $(this).find("input[type=hidden]").val();
        var pageForSearch = $("#searchFullForm input[name='searchParams.pageForSearch']");
        pageForSearch.val(selectedValue);

        $("#searchFullForm").submit();
        return false;
    });

    $('#city').bind('change', function (evt) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        var cityIdForSearch = $("#searchFullForm input[name='searchParams.cityIdForSearch']");
        cityIdForSearch.val(valueSelected);
        $("#searchFullForm input[name='searchParams.pageForSearch']").val(1);

        $("#searchFullForm").submit();
    });

    $('#cost').bind('change', function (evt) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        var costIdForSearch = $("#searchFullForm input[name='searchParams.costIdForSearch']");
        costIdForSearch.val(valueSelected);
        $("#searchFullForm input[name='searchParams.pageForSearch']").val(1);

        $("#searchFullForm").submit();
    });

    $('#sortByField').bind('change', function (evt) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        var costIdForSearch = $("#searchFullForm input[name='searchParams.sortByFieldForSearch']");
        costIdForSearch.val(valueSelected);
        $("#searchFullForm input[name='searchParams.pageForSearch']").val(1);

        $("#searchFullForm").submit();
    });

    $(".passValueToHiddenInputCheckBox").bind("click", function (evt) {
        var hidden = $(this).parent().find("input[type=hidden]");
        hidden.val($(this).prop('checked'));
    });

    // Automatic highlight current page in top navbar
    {
        var href = location.href.toLowerCase().replace(/#$/, '');
        $("#topnav li.active").removeClass("active");
        $("#topnav li a").filter(function() {
            return this.href.toLowerCase() === href;
        }).each(function() {
           $(this).parent("li").addClass("active");
        });
    }
});