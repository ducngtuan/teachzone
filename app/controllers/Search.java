package controllers;

import logic.PagingLogic;
import logic.SearchLogic;
import pojos.SearchParams;
import models.*;
import play.db.jpa.GenericModel;
import play.mvc.Before;
import play.mvc.Controller;
import pojos.UserType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Search extends BaseController {
    @Before
    static void checkAuthenticationAndPrepareCommonData() {
        List<String> sortByFields = new ArrayList<String>(Arrays.asList(
                "Fullname", "Rating"));
        renderArgs.put("sortByFields", sortByFields);
    }

    public static void index() {
        SearchParams searchParams = getDefaultSearchParams();
        operateSearch(searchParams);
        render();
    }

    public static void search(SearchParams searchParams) {
        if (searchParams == null){
            searchParams = getDefaultSearchParams();
        }
        if (searchParams.pageForSearch == 0){
            searchParams.pageForSearch = 1;
        }
        operateSearch(searchParams);
        renderTemplate("Search/index.html");
    }

    public static void searchBySubject(int id) {
        SearchParams sp = getDefaultSearchParams();
        sp.subjectIdForSearch = id;
        operateSearch(sp);
        renderTemplate("Search/index.html");
    }

    private static void operateSearch(SearchParams searchParams) {
        setTemplateVariables(searchParams);
        GenericModel.JPAQuery result = new SearchLogic().getSearchModel(searchParams, (List<Category>)renderArgs.get("categories"), (List<UserType>)renderArgs.get("userTypes"));
        setPagingVariables(result, searchParams.pageForSearch);
    }

    private static SearchParams getDefaultSearchParams() {
        SearchParams searchParams = new SearchParams();
        searchParams.userTypeForSearch = "All";
        searchParams.sortByFieldForSearch = "Rating";
        searchParams.pageForSearch = 1;
        return searchParams;
    }

    private static void setTemplateVariables(SearchParams searchParams) {
        if (searchParams != null) {
            renderArgs.put("userTypeForSearch", searchParams.userTypeForSearch);
            renderArgs.put("textForSearch", searchParams.textForSearch);
            renderArgs.put("subjectIdForSearch", searchParams.subjectIdForSearch);
            renderArgs.put("cityIdForSearch", searchParams.cityIdForSearch);
            renderArgs.put("costIdForSearch", searchParams.costIdForSearch);
            renderArgs.put("pageForSearch", searchParams.pageForSearch);
            renderArgs.put("sortByFieldForSearch", searchParams.sortByFieldForSearch);
        }
    }

    private static void setPagingVariables(GenericModel.JPAQuery results, int pageForSearch) {
        PagingLogic logic = new PagingLogic().getPagingModel(results, pageForSearch);
        renderArgs.put("nextDisabled", logic.nextDisabled);
        renderArgs.put("prevDisabled", logic.prevDisabled);
        renderArgs.put("pageForSearch", pageForSearch);
        renderArgs.put("allResultsCount", logic.allResultsCount);
        renderArgs.put("pages", logic.pages);
        renderArgs.put("pageResults", logic.pageResults);

    }
}