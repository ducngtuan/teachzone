package controllers;

import models.User;
import play.mvc.Before;
import play.mvc.Controller;

public class UserAccount extends Controller {
    @Before
    public static void prepareLoggedInUserRenderArgs() {
        renderArgs.put("loggedIn", isLoggedIn());
        renderArgs.put("loggedInUser", getLoggedInUser());
    }

    public static User getLoggedInUser() {
        if (!isLoggedIn()) return null;

        long id = Long.parseLong(session.get("loggedInUserId"));
        return User.findById(id);
    }

    public static boolean isLoggedIn() {
        return session.contains("loggedInUserId");
    }

    public static boolean isLoggedIn(User user) {
        return session.contains("loggedInUserId") &&
                session.get("loggedInUserId").equals(user.id.toString());
    }

    public static void index() {
        render();
    }

    public static void login(String email, String password) {
        User user = User.find("byEmail", email).first();
        if (user == null || !user.authenticate(password)) {
            flash.error("Incorrect email or password!");
            index();
        } else {
            session.put("loggedInUserId", user.id);

            if(session.get("isSearchedLogin") != null && Boolean.parseBoolean(session.get("isSearchedLogin")) == true ) {
                if(session.get("searchedUserID") != null){
                    Users.show(Long.parseLong(session.get("searchedUserID")));
                }
            }
            else {
                session.put("isSearchedLogin", false);
                UserProfile.index();
            }
        }
    }

    public static void logout() {
        session.clear();
        Application.index();
    }

    public static void forgotPassword() {
        render();
    }

    //user gets registration form
    public static void signup() {
        render();
    }

    public static void searchLogin() {
        session.put("isSearchedLogin",true);
        index();
    }
//    public static void createAccount(UserInfo user) {
//        if (user != null) {
//            validation.required(user.firstname).message("Please enter your first name.").key("firstname");
//            validation.required(user.surname).message("Please enter your last name.").key("surname");
//            validation.required(user.postalCode).message("Please enter your postal address.").key("postalCode");
//            validation.required(user.city).message("Please enter your city.").key("city");
//            validation.required(user.address).message("Please enter your address.").key("address");
//            validation.required(user.email).message("Please enter your email address.").key("email");
//            validation.required(user.password).message("Please enter your password.").key("password");
//            validation.required(user.cpassword).message("Please type the password again.").key("cpassword");
//            validation.equals(user.password, user.cpassword).message("Mismatched password.").key("mismatch");
//            if (validation.hasErrors()) {
//                params.flash(); // add http parameters to the flash scope
//                //validation.keep(); // keep the errors for the next request
//                for (Error error : validation.errors()) {
//                    System.out.println(error.getKey());
//                    flash.put("error_" + error.getKey(), error.message());
//
//                }
//            } else {
//                User newuser = new User();
//                StudentInformation student = new StudentInformation();
//                TutorInformation tutor = new TutorInformation();
//                City city = new City();
//                newuser.email = user.email;
//                newuser.password = user.password;
//                newuser.fullName = user.firstname + " " + user.surname;
//                newuser.postalCode = user.postalCode;
//
//
//                newuser.address = user.address;
//                newuser.genderCode = (user.gender.equals("male")) ? 1 : 2;
//
//                try {
//                    city.name = user.city;
//
//                    if (user.userType.equals("student")) {
//                        student.user = newuser;
//                        newuser.create();
//                        student.create();
//                        //city.create();
//                        //city.users.add(newuser);
//                        //User testUser = User.find("byEmail",user.email).first();
//
//                    } else if (user.userType.equals("tutor")) {
//                        tutor.user = newuser;
//                        newuser.create();
//                        tutor.create();
//                        //city.users.add(newuser);
//                        //city.create();
//                        //User testUser = User.find("byEmail",user.email).first();
//
//                    }
//
//                } catch (Exception e1) {
//                    e1.printStackTrace();
//                }
//
//            }
//        }
//
//        UserAccount.signup();
//    }
}
