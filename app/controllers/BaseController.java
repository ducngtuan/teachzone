/**
 * Copyright 2014 Technical University Munich, Software Engineering for Business Information Systems (sebis)
 * <p/>
 * Created by Duc Nguyen on 2014.06.11.
 */
package controllers;

import play.mvc.Controller;
import play.mvc.With;

@With({CategoriesSidebar.class, UserAccount.class})
public class BaseController extends Controller {
}
