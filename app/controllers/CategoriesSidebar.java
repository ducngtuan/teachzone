package controllers;

import models.Category;
import models.City;
import models.Cost;
import play.mvc.Before;
import play.mvc.Controller;
import pojos.UserType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoriesSidebar extends Controller {
    @Before
    static void prepareDataForCategoriesSidebar() {
        List<UserType> userTypes = new ArrayList<UserType>(Arrays.asList(
                new UserType("Students", 0), new UserType("Tutors", 0
                )));
        renderArgs.put("userTypes", userTypes);
        renderArgs.put("categories", Category.findAll());
        renderArgs.put("cities", City.findAll());
        renderArgs.put("costs", Cost.findAll());
    }
}
