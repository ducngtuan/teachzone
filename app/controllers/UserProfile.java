package controllers;

import models.*;
import org.joda.time.DateTime;
import play.data.validation.*;
import play.data.validation.Error;
import play.mvc.Before;

import java.util.List;

public class UserProfile extends BaseController {

    /**
     * @return the currently logged in user
     */
    static User getCurrentUser() {
        return UserAccount.getLoggedInUser();
    }

    /**
     * Get the current logged in user and put it to `renderArgs`
     * so it will be used by all action in this controller.
     */
    @Before
    static void passCurrentUserToRenderArgs() {
        if (!UserAccount.isLoggedIn())
            Application.index();

        User user = getCurrentUser();
        renderArgs.put("user", user);

        List<SchoolType> schoolTypes = SchoolType.findAll();
        List<ClassLevel> classLevels = ClassLevel.findAll();
        List<Subject> subjects = Subject.findAll();
        List<Cost> costs = Cost.findAll();
        List<Qualification> qualifications = Qualification.findAll();
        List<TutoringExperience> tutoringExperiences = TutoringExperience.findAll();

        renderArgs.put("schoolTypes", schoolTypes);
        renderArgs.put("classLevels", classLevels);
        renderArgs.put("subjects", subjects);
        renderArgs.put("costs", costs);
        renderArgs.put("qualifications", qualifications);
        renderArgs.put("tutoringExperiences", tutoringExperiences);
    }

    /**
     * Render the user profile page.
     */
    public static void index() {
        //renderArgs.put("user", getCurrentUser());
        render();
    }

    /**
     * Render the form for update general user information.
     */
    public static void edit() {
        List<City> cities = City.findAll();
        render(cities);
    }

    /**
     * Update the user profile information.
     */
    public static void update(@Valid User userFromBrowser) {
        User user = getCurrentUser();
        user.fullName = userFromBrowser.fullName;
        user.description = userFromBrowser.description;
        user.genderCode = userFromBrowser.genderCode;
        user.dateOfBirth = userFromBrowser.dateOfBirth;
        user.postalCode = userFromBrowser.postalCode;
        user.city = City.findById(userFromBrowser.cityId);
        user.address = userFromBrowser.address;
        user.telephoneNumber = userFromBrowser.telephoneNumber;
        user.imageURL = userFromBrowser.imageURL;

        // TODO replace dateOfBirth with day, month, year so that we can validate them separately
        if (Validation.hasErrors()) {
            //if (userFromBrowser.dateOfBirth != null)
            //    flash.put("studentInformationFromBrowser.dateOfBirth", userFromBrowser.dateOfBirth.toLocalDate().toString());
            renderArgs.put("user", user);
            renderTemplate("UserProfile/edit.html");
        }

        user.save();
        flash.success("Profile information successfully updated!");
        index();
    }

    /**
     * Render the form for update student information.
     */
    public static void editStudentInfo() {
        User user = getCurrentUser();

        if (user.studentInformation == null)
            user.studentInformation = new StudentInformation(user);
        if (user.studentInformation.preferredTime == null || user.studentInformation.preferredTime.length != 28){
            user.studentInformation.preferredTime = new boolean[28];
        }
        render();
    }

    /**
     * Update the student information.
     */
    public static void updateStudentInfo(@Valid StudentInformation studentInformationFromBrowser) {
        User user = getCurrentUser();
        if (user.studentInformation == null)
            user.studentInformation = new StudentInformation(user);

        user.studentInformation.schoolType = SchoolType.findById(studentInformationFromBrowser.schoolTypeId);
        user.studentInformation.classLevel = ClassLevel.findById(studentInformationFromBrowser.classLevelId);
        user.studentInformation.descriptionStudent = studentInformationFromBrowser.descriptionStudent;
        user.studentInformation.tutoringChannelOffline = studentInformationFromBrowser.tutoringChannelOffline;
        user.studentInformation.tutoringChannelOnline = studentInformationFromBrowser.tutoringChannelOnline;
        user.studentInformation.tutoringPlaceStudentHome = studentInformationFromBrowser.tutoringPlaceStudentHome;
        user.studentInformation.tutoringPlaceTutorHome = studentInformationFromBrowser.tutoringPlaceTutorHome;
        user.studentInformation.preferredTime = studentInformationFromBrowser.preferredTime;
        Validation.isTrue("studentInformationFromBrowser.tutoringChannel", user.studentInformation.tutoringChannelOffline || user.studentInformation.tutoringChannelOnline).message("validation.selectAtLeastOne");
        Validation.isTrue("studentInformationFromBrowser.tutoringPlace", user.studentInformation.tutoringPlaceStudentHome || user.studentInformation.tutoringPlaceTutorHome).message("validation.selectAtLeastOne");

        if (Validation.hasErrors()) {
            renderArgs.put("user", user);
            renderTemplate("UserProfile/editStudentInfo.html");
        }

        user.studentInformation.save();
        flash.success("Student information successfully updated!");
        index();
    }

    /**
     * Delete the student information. The user will no longer be a student.
     */
    public static void deleteStudentInfo() {
        User user = getCurrentUser();
        user.studentInformation.delete();
        index();
    }

    public static void addCourseSearch(
            @Required @Min(value = 1, message = "validation.required") long subjectId,
            @Required @Min(value = 1) int desiredGroupSize,
            @Required @Min(value = 1, message = "validation.required") long costId) {
        User user = getCurrentUser();
        StudentInformation si = user.studentInformation;
        Subject subject = Subject.findById(subjectId);
        Cost cost = Cost.findById(costId);

        if (Validation.hasErrors()) {
            params.flash();
            Validation.keep();
            editStudentInfo();
        }

        CourseSearch cs = new CourseSearch(si, subject, cost, desiredGroupSize);
        cs.save();
        editStudentInfo();
    }

    public static void removeCourseSearch(Long id) {
        CourseSearch cs = CourseSearch.findById(id);
        cs.delete();
        editStudentInfo();
    }

    /**
     * Render the form for update student information.
     */
    public static void editTutorInfo() {
        User user = getCurrentUser();


        if (user.tutorInformation == null)
            user.tutorInformation = new TutorInformation(user);
        if (user.tutorInformation.preferredTime == null || user.tutorInformation.preferredTime.length != 28){
            user.tutorInformation.preferredTime = new boolean[28];
        }
        render();
    }

    /**
     * Update the tutor information.
     */
    public static void updateTutorInfo(@Valid TutorInformation tutorInformationFromBrowser) {
        User user = getCurrentUser();
        if (user.tutorInformation == null)
            user.tutorInformation = new TutorInformation(user);

        user.tutorInformation.qualification = Qualification.findById(tutorInformationFromBrowser.qualificationId);
        user.tutorInformation.tutoringExperience = TutoringExperience.findById(tutorInformationFromBrowser.tutoringExperienceId);
        user.tutorInformation.descriptionTutor = tutorInformationFromBrowser.descriptionTutor;
        user.tutorInformation.tutoringChannelOffline = tutorInformationFromBrowser.tutoringChannelOffline;
        user.tutorInformation.tutoringChannelOnline = tutorInformationFromBrowser.tutoringChannelOnline;
        user.tutorInformation.tutoringPlaceStudentHome = tutorInformationFromBrowser.tutoringPlaceStudentHome;
        user.tutorInformation.tutoringPlaceTutorHome = tutorInformationFromBrowser.tutoringPlaceTutorHome;
        user.tutorInformation.preferredTime = tutorInformationFromBrowser.preferredTime;

        Validation.isTrue("tutorInformationFromBrowser.tutoringChannel", user.tutorInformation.tutoringChannelOffline || user.tutorInformation.tutoringChannelOnline).message("validation.selectAtLeastOne");
        Validation.isTrue("tutorInformationFromBrowser.tutoringPlace", user.tutorInformation.tutoringPlaceStudentHome || user.tutorInformation.tutoringPlaceTutorHome).message("validation.selectAtLeastOne");

        if (Validation.hasErrors()) {
            renderArgs.put("user", user);
            renderTemplate("UserProfile/editTutorInfo.html");
        }

        user.tutorInformation.save();
        flash.success("Tutor information successfully updated!");
        index();
    }

    /**
     * Delete the tutor information. The user will no longer be a tutor.
     */
    public static void deleteTutorInfo() {
        User user = getCurrentUser();
        user.tutorInformation.delete();
        index();
    }

    public static void addCourseOffer(
            @Required @Min(value = 1, message = "validation.required") long subjectId,
            @Required @Min(value = 1) int desiredGroupSize,
            @Required @Min(value = 1, message = "validation.required") long costId) {
        User user = getCurrentUser();
        TutorInformation ti = user.tutorInformation;
        Subject subject = Subject.findById(subjectId);
        Cost cost = Cost.findById(costId);

        if (Validation.hasErrors()) {
            params.flash();
            Validation.keep();
            editTutorInfo();
        }

        CourseOffer co = new CourseOffer(ti, subject, cost, desiredGroupSize);
        co.save();
        editTutorInfo();
    }

    public static void removeCourseOffer(Long id) {
        CourseOffer co = CourseOffer.findById(id);
        co.delete();
        editTutorInfo();
    }

    public static void showSearchedUser(Long id){
        if (!UserAccount.isLoggedIn())
            Application.index();

        User user = User.findById(id);
        renderArgs.put("user", user);

        List<SchoolType> schoolTypes = SchoolType.findAll();
        List<ClassLevel> classLevels = ClassLevel.findAll();
        List<Subject> subjects = Subject.findAll();
        List<Cost> costs = Cost.findAll();
        List<Qualification> qualifications = Qualification.findAll();
        List<TutoringExperience> tutoringExperiences = TutoringExperience.findAll();

        renderArgs.put("schoolTypes", schoolTypes);
        renderArgs.put("classLevels", classLevels);
        renderArgs.put("subjects", subjects);
        renderArgs.put("costs", costs);
        renderArgs.put("qualifications", qualifications);
        renderArgs.put("tutoringExperiences", tutoringExperiences);
    }
}
