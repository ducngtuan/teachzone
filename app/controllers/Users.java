/**
 * Copyright 2014 Technical University Munich, Software Engineering for Business Information Systems (sebis)
 * <p/>
 * Created by Duc Nguyen on 2014.06.10.
 */
package controllers;

import models.User;

public class Users extends BaseController {
    public static void show(Long id) {
        User user = User.findById(id);

        //searchedUserID will be used to show searched user profile immediately after login
        if(user != null){
            session.put("searchedUserID",id);
            session.put("isSearchedLogin",false);
        }
        if (user.studentInformation != null
                && (user.studentInformation.preferredTime == null || user.studentInformation.preferredTime.length != 28)){
            user.studentInformation.preferredTime = new boolean[28];
        }
        if (user.tutorInformation != null
                && (user.tutorInformation.preferredTime == null || user.tutorInformation.preferredTime.length != 28)){
            user.tutorInformation.preferredTime = new boolean[28];
        }
        render("UserProfile/index.html", user);
    }
}
