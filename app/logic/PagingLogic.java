package logic;

import play.db.jpa.GenericModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Emin Guliyev on 30/05/2014.
 */
public class PagingLogic {
    public List<Integer> pages = new ArrayList();
    public int pageCapacity = 10;
    public int maxPagingIndexesCount = 5;
    public int allResultsCount = 0;
    public boolean nextDisabled = true;
    public boolean prevDisabled = true;
    public List pageResults=new ArrayList();

    public PagingLogic getPagingModel(GenericModel.JPAQuery results, int pageForSearch) {
        if (results.fetch().isEmpty()) {
            pages = Arrays.asList(1);
        } else
        {
            //page indexes in paging control
            allResultsCount = results.fetch().size();
            int pageCount = allResultsCount / pageCapacity;
            if (allResultsCount % pageCapacity > 0) {
                pageCount++;
            }
            int firstPagingIndex = pageForSearch + 1
                    - (pageForSearch % maxPagingIndexesCount == 0 ? maxPagingIndexesCount : pageForSearch % maxPagingIndexesCount);
            int lastPagingIndex = firstPagingIndex + maxPagingIndexesCount - 1;
            if (lastPagingIndex > pageCount) {
                lastPagingIndex = pageCount;
            }
            for (int i = firstPagingIndex; i <= lastPagingIndex; i++) {
                pages.add(i);
            }
            //page results in view
            int firstResultIndexInPage = (pageForSearch - 1) * pageCapacity + 1;
            int lastResultIndexInPage = firstResultIndexInPage + pageCapacity - 1;
            if (lastResultIndexInPage > allResultsCount) {
                lastResultIndexInPage = allResultsCount;
            }
            int pageResultsCounts = lastResultIndexInPage + 1 - firstResultIndexInPage;
            pageResults = results.from(firstResultIndexInPage - 1).fetch(pageResultsCounts);
            //next prev buttons
            if (firstPagingIndex > 1) {
                prevDisabled = false;
            }
            if (lastPagingIndex < pageCount) {
                nextDisabled = false;
            }
        }
        return this;
    }

}
