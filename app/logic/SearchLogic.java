package logic;

import models.Category;
import models.Subject;
import models.User;
import pojos.SearchParams;
import pojos.UserType;
import play.db.jpa.GenericModel;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Emin Guliyev on 30/05/2014.
 */
public class SearchLogic {
    public SearchParams searchParams;
    private String fieldsToSelect = " DISTINCT u ";
    private String from = " FROM User u LEFT OUTER JOIN u.tutorInformation.courseOffers courseOffers LEFT OUTER JOIN u.studentInformation.courseSearches courseSearches LEFT OUTER JOIN courseOffers.subject courseOffersSubject LEFT OUTER JOIN courseSearches.subject courseSearchesSubject LEFT OUTER JOIN u.city city ";
    private String fromWhere = "";
    private String select = " SELECT ";
    private String where = " WHERE ";
    private String orderBy = "";
    private String conditions = "";
    private HashMap<Long, Subject> subjectMap = new HashMap<Long, Subject>();
    private HashMap<String, UserType> userTypeMap = new HashMap<String, UserType>();
    public GenericModel.JPAQuery getSearchModel( SearchParams searchParams, List<Category> categories, List<UserType> userTypes){
        fillMaps(categories, userTypes);
        GenericModel.JPAQuery result = null;
        if (searchParams == null){
            result = User.all();
        }else {
            this.searchParams = searchParams;

            userTypeFilter();
            keywordFilter();
            cityFilter();
            subjectFilter();
            costFilter();
            sorting();

            if (conditions.isEmpty()) {
                fromWhere = from;
            } else {
                fromWhere = from + where + conditions.trim();
            }
        }

        setCategoryCounts();
        setUserTypeCounts();
        result = User.find(select + fieldsToSelect + fromWhere + orderBy);
        return result;
    }

    private void keywordFilter() {
        if (!isNullOrEmpty(searchParams.textForSearch)){
            addToQuery(" ((UPPER(u.fullName) LIKE '%" + searchParams.textForSearch.toUpperCase() + "%') "
                    + " OR "
                    + " (UPPER(courseOffersSubject.name) LIKE '%" + searchParams.textForSearch.toUpperCase() + "%') "
                    + " OR "
                    + " (UPPER(city.name) LIKE '%" + searchParams.textForSearch.toUpperCase() + "%') "
                    + " OR "
                    + " (UPPER(courseSearchesSubject.name) LIKE '%" + searchParams.textForSearch.toUpperCase() + "%')) ");
        }
    }

    private void fillMaps(List<Category> categories, List<UserType> userTypes) {
        for (Category category: categories){
            for (Subject subject: category.subjects){
                this.subjectMap.put(subject.id, subject);
            }
        }
        for(UserType userType: userTypes){
            this.userTypeMap.put(userType.name, userType);
        }
    }

    private void setUserTypeCounts() {
        String newFromWhere = fromWhere;
        if (!fromWhere.contains(where)){
            newFromWhere += where;
        }else{
            newFromWhere += " AND ";
        }
        String queryTutors = "SELECT COUNT(DISTINCT u) as c " + newFromWhere + " (u.tutorInformation.id IS NOT NULL)";
        String queryStudents = "SELECT COUNT(DISTINCT u) as c " + newFromWhere + " (u.studentInformation.id IS NOT NULL)";

        Long countTutors = User.find(queryTutors).first();
        Long countStudents = User.find(queryStudents).first();
        UserType tutorsUserType = (UserType)userTypeMap.get("Tutors");
        tutorsUserType.count = countTutors;
        UserType studentsUserType = (UserType)userTypeMap.get("Students");
        studentsUserType.count = countStudents;
    }

    private void setCategoryCounts() {
        String queryOffers = " SELECT courseOffersOuter.subject.id as subject_id, COUNT(*) as c FROM  User uOuter JOIN uOuter.tutorInformation.courseOffers courseOffersOuter WHERE uOuter.id IN (SELECT DISTINCT u.id " + fromWhere + ") GROUP BY courseOffersOuter.subject.id " ;
        String querySearches = " SELECT courseSearchesOuter.subject.id as subject_id, COUNT(*) as c FROM  User uOuter JOIN uOuter.studentInformation.courseSearches courseSearchesOuter WHERE uOuter.id IN (SELECT DISTINCT u.id " + fromWhere + ") GROUP BY courseSearchesOuter.subject.id ";

        operateSubjectCountQuery(queryOffers);
        operateSubjectCountQuery(querySearches);
    }

    private void operateSubjectCountQuery(String query) {
        List res = User.find(query).fetch();
        for (Object couple: res){
            Object[] coupleArray = (Object[])couple;
            if (coupleArray.length == 2 && coupleArray[0] != null && coupleArray[1] != null){
                findSubjectAndIncCount((Long) coupleArray[0], (Long) coupleArray[1]);
            }
        }
    }

    private void findSubjectAndIncCount(long subjectId, long count) {
        Subject subject = subjectMap.get(subjectId);
        subject.count += count;
    }

    private void sorting() {
        if ("Fullname".equals(searchParams.sortByFieldForSearch)){
            orderBy = " ORDER BY u.fullName ";
        }
    }

    private void costFilter() {
        if (searchParams.costIdForSearch > 0){
            String tutorCostFilter = " (courseOffers.cost.id = " + searchParams.costIdForSearch + ") ";
            String userCostFilter = " (courseSearches.cost.id = " + searchParams.costIdForSearch + ") ";
            if (searchParams.userTypeForSearch.equals("Tutors")) {
                addToQuery(tutorCostFilter);
            }
            if (searchParams.userTypeForSearch.equals("Students")) {
                addToQuery(userCostFilter);
            }
            if (searchParams.userTypeForSearch.equals("All")) {
                addToQuery(" (" + tutorCostFilter + " OR " + userCostFilter + ") ");
            }
        }
    }

    private void subjectFilter() {
        if (searchParams.subjectIdForSearch > 0) {
            String tutorSubjectFilter = " (courseOffers.subject.id = " + searchParams.subjectIdForSearch + ") ";
            String userSubjectFilter = " (courseSearches.subject.id = " + searchParams.subjectIdForSearch + ") ";
            if (searchParams.userTypeForSearch.equals("Tutors")) {
                addToQuery(tutorSubjectFilter);
            }
            if (searchParams.userTypeForSearch.equals("Students")) {
                addToQuery(userSubjectFilter);
            }
            if (searchParams.userTypeForSearch.equals("All")) {
                addToQuery(" (" + tutorSubjectFilter + " OR " + userSubjectFilter + ") ");
            }
        }
    }

    private void cityFilter() {
        if (searchParams.cityIdForSearch > 0){
            addToQuery(" ( u.city.id=" + searchParams.cityIdForSearch + " ) ");
        }
    }

    private void addToQuery(String condition) {
        if (isNullOrEmpty(conditions)){
            conditions = condition;
        }else{
            conditions += " AND " + condition;
        }
    }

    private void userTypeFilter() {
        if (isNullOrEmpty(searchParams.userTypeForSearch)){
            searchParams.userTypeForSearch = "All";
        }

        if (searchParams.userTypeForSearch.equals("Students")){
            conditions += " (u.studentInformation.id IS NOT NULL) ";
        }

        if (searchParams.userTypeForSearch.equals("Tutors")){
            conditions += " (u.tutorInformation.id IS NOT NULL) ";
        }
    }

    private boolean isNullOrEmpty(String value){
        return value == null || value.isEmpty();
    }
}
