package pojos;

import java.util.ArrayList;

/**
 * Created by Emin Guliyev on 29/05/2014.
 */
public class UserType {
    public String name;
    public long count;

    public UserType(String name, int count) {
        this.name = name;
        this.count = count;
    }
}
