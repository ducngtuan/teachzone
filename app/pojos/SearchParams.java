package pojos;

/**
 * Created by Emin Guliyev on 29/05/2014.
 */
public class SearchParams {
    public String userTypeForSearch;
    public String textForSearch;
    public int subjectIdForSearch;
    public int cityIdForSearch;
    public int costIdForSearch;
    public int pageForSearch;
    public String sortByFieldForSearch;
}
