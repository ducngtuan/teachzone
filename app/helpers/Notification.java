package helpers;

import play.libs.Mail;
import org.apache.commons.mail.*;
/**
 * Created by jitendra on 6/9/2014.
 */
public class Notification {
    public static void sendMail(String to,String messageCode){
        try {
            SimpleEmail uemail = new SimpleEmail();
            uemail.setFrom("info@teachzone.com");
            uemail.addTo(to);
            uemail.setSubject("TeachZone Registration.");
            uemail.setMsg("Welcome to TeachZone !!");
            Mail.send(uemail);
        }catch(Exception e1){
            e1.printStackTrace();
        }
    }
}
