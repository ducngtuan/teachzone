import models.*;
import org.joda.time.DateTime;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

import java.util.List;
import java.util.Random;

@OnApplicationStart
public class Bootstrap extends Job {
    public void doJob() {
        Fixtures.deleteDatabase();
        Fixtures.loadModels("initial-data.yml");

        // int user = (int)User.count();
        List<User> userList = User.findAll();
        for (int i = 0; i < User.count(); i++) {
            User user = userList.get(i);
            user.updatePassword(user.password);
            user.save();
        }

        // generate random user
        UserGenerator ug = new UserGenerator();
        for (int i = 0; i < new Random(System.currentTimeMillis()).nextInt(20) + 20; i++)
            ug.generateUser();
    }

    class UserGenerator {
        private String[] firstNames = {"Jennefer", "Tyron", "Eugenie", "Irving", "Edgar", "Catina", "Denae", "Ettie", "Arthur", "Levi", "Kattie", "Caprice", "Lera", "Hilary", "Ilona", "Freida", "Clifton", "Nolan", "Sonia", "Tamesha"};
        private String[] lastNames = {"Flowers", "Meyer", "Adams", "Tran", "Frost", "Lambert", "Ray", "Cline", "Houston", "Bright"};
        private List<City> cities = City.findAll();
        private List<Subject> subjects = Subject.findAll();
        private List<Qualification> qualifications = Qualification.findAll();
        private List<TutoringExperience> experiences = TutoringExperience.findAll();
        private List<Cost> costs = Cost.findAll();
        private List<SchoolType> schoolTypes = SchoolType.findAll();
        private List<ClassLevel> classLevels = ClassLevel.findAll();
        private Random random = new Random(System.currentTimeMillis());

        private <T> T chooseOne(List<T> list) {
            return list.get(random.nextInt(list.size()));
        }

        private String randomText(int length) {
            String lorem = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer";
            int begin = random.nextInt(lorem.length() - length);
            return lorem.substring(begin, begin + length);
        }

        public void generateUser() {
            String firstName = firstNames[random.nextInt(firstNames.length)];
            String lastName = lastNames[random.nextInt(lastNames.length)];
            String email = System.currentTimeMillis() + "@foobar.com";
            User user = new User(email, firstName + " " + lastName, "123456");
            user.city = chooseOne(cities);
            user.dateOfBirth = DateTime.now().minusYears(random.nextInt(50));
            user.description = randomText(random.nextInt(200));
            user.genderCode = random.nextInt(2) + 1;
            user.address = "Musterstr. " + random.nextInt(50);
            user.postalCode = Integer.valueOf(random.nextInt(9000) + 1000).toString();
            user.save();

            double rnd = random.nextDouble();
            if (rnd < 0.1) {
                // user are neither student nor tutor
            } else if (rnd < 0.45) {
                generateStudentInformation(user);
            } else if (rnd < 0.8) {
                generateTutorInformation(user);
            } else {
                generateStudentInformation(user);
                generateTutorInformation(user);
            }
        }

        private void generateTutorInformation(User user) {
            TutorInformation ti = new TutorInformation(user);
            ti.descriptionTutor = randomText(random.nextInt(140));
            ti.qualification = chooseOne(qualifications);
            ti.tutoringExperience = chooseOne(experiences);
            ti.tutoringChannelOffline = random.nextBoolean();
            ti.tutoringChannelOnline = !ti.tutoringChannelOffline || random.nextBoolean();
            ti.tutoringPlaceStudentHome = random.nextBoolean();
            ti.tutoringPlaceTutorHome = !ti.tutoringPlaceStudentHome || random.nextBoolean();
            ti.save();

            for (int i = 0; i < random.nextInt(5) + 1; i++) {
                CourseOffer co = new CourseOffer(ti, chooseOne(subjects), chooseOne(costs), random.nextInt(10) + 1);
                co.save();
                ti.courseOffers.add(co);
            }
        }

        private void generateStudentInformation(User user) {
            StudentInformation si = new StudentInformation(user);
            si.descriptionStudent = randomText(random.nextInt(140));
            si.schoolType = chooseOne(schoolTypes);
            si.classLevel = chooseOne(classLevels);
            si.tutoringChannelOffline = random.nextBoolean();
            si.tutoringChannelOnline = !si.tutoringChannelOffline || random.nextBoolean();
            si.tutoringPlaceStudentHome = random.nextBoolean();
            si.tutoringPlaceTutorHome = !si.tutoringPlaceStudentHome || random.nextBoolean();
            si.save();

            for (int i = 0; i < random.nextInt(5) + 1; i++) {
                CourseSearch cs = new CourseSearch(si, chooseOne(subjects), chooseOne(costs), random.nextInt(10) + 1);
                cs.save();
                si.courseSearches.add(cs);
            }
        }
    }
}
