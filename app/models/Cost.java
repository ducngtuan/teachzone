package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by Emin Guliyev on 29/05/2014.
 */
@Entity
public class Cost extends Model{
    public String name;
    public int min;
    public int max;

    @OneToMany(mappedBy = "cost", cascade = CascadeType.ALL)
    public List<CourseOffer> offerCosts;

    @OneToMany(mappedBy = "cost", cascade = CascadeType.ALL)
    public List<CourseSearch> searchCost;

    public Cost(String name, int min, int max) {
        this.name = name;
        this.min = min;
        this.max = max;
    }
}
