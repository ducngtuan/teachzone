package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emin Guliyev on 29/05/2014.
 */
@Entity
public class City extends Model{
    public String name;

    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL)
    public List<User> users;

    public City(String name) {
        this.name = name;
    }

    public City() {

    }
}
