package models;


import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Category extends Model {
    public String name;

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    public List<Subject> subjects;

    public Category(String name) {
        this.subjects = new ArrayList<Subject>();
        this.name = name;
    }
}
