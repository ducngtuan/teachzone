package models;


import play.data.validation.MaxSize;
import play.data.validation.Min;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TutorInformation extends Model {
    @OneToOne
    public User user;

    @ManyToOne
    public Qualification qualification;

    @ManyToOne
    public TutoringExperience tutoringExperience;
    @MaxSize(value = 255)
    public String descriptionTutor;
    public boolean tutoringChannelOffline;
    public boolean tutoringChannelOnline;
    public boolean tutoringPlaceStudentHome;
    public boolean tutoringPlaceTutorHome;

    @OneToMany(mappedBy = "tutor", cascade = CascadeType.ALL)
    public List<CourseOffer> courseOffers;

    public boolean[] preferredTime = new boolean[28];

    @Transient
    @Required
    @Min(value = 1, message = "validation.required")
    public long qualificationId;
    @Transient
    @Required
    @Min(value = 1, message = "validation.required")
    public long tutoringExperienceId;

    public TutorInformation(User user) {
        this.courseOffers = new ArrayList<CourseOffer>();
        this.user = user;
    }
}
