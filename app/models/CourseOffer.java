package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class CourseOffer extends Model {
    @ManyToOne
    public TutorInformation tutor;

    @ManyToOne
    public Subject subject;

    @ManyToOne
    public Cost cost;

    public int desiredGroupSize;

    public CourseOffer(TutorInformation tutor, Subject subject, Cost cost, int desiredGroupSize) {
        this.tutor = tutor;
        this.subject = subject;
        this.cost = cost;
        this.desiredGroupSize = desiredGroupSize;
    }
}
