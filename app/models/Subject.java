package models;


import play.db.jpa.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Subject extends Model {
    public String name;

    @ManyToOne
    public Category category;

    @OneToMany(mappedBy = "subject", cascade = CascadeType.ALL)
    public List<CourseOffer> courseOffers;

    @OneToMany(mappedBy = "subject", cascade = CascadeType.ALL)
    public List<CourseSearch> courseSearches;

    @Transient
    public long count;

    public Subject(Category category, String name) {
        this.courseOffers = new ArrayList<CourseOffer>();
        this.courseSearches = new ArrayList<CourseSearch>();
        this.name = name;
        this.category = category;
    }
}
