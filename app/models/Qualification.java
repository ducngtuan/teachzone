/**
 * Created by Emin Guliyev on 29/05/2014.
 */
package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Qualification extends Model {
    public String name;

    @OneToMany(mappedBy = "qualification", cascade = CascadeType.ALL)
    public List<TutorInformation> tutors;

    public Qualification(String name) {
        this.name = name;
    }
}