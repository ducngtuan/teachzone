package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class TutoringExperience extends Model {
    public String name;

    @OneToMany(mappedBy = "tutoringExperience", cascade = CascadeType.ALL)
    public List<TutorInformation> tutors;

    public TutoringExperience(String name) {
        this.name = name;
    }
}