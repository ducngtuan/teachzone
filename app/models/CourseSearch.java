package models;


import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class CourseSearch extends Model {
    @ManyToOne
    public StudentInformation student;

    @ManyToOne
    public Subject subject;

    @ManyToOne
    public Cost cost;

    public int desiredGroupSize;

    public CourseSearch(StudentInformation student, Subject subject, Cost cost, int desiredGroupSize) {
        this.student = student;
        this.subject = subject;
        this.cost = cost;
        this.desiredGroupSize = desiredGroupSize;
    }
}
