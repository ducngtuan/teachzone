package models;


import play.data.validation.MaxSize;
import play.data.validation.Min;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class StudentInformation extends Model {
    @OneToOne
    public User user;

    @ManyToOne
    public SchoolType schoolType;

    @ManyToOne
    public ClassLevel classLevel;
    @MaxSize(value = 255)
    public String descriptionStudent;

    public boolean tutoringChannelOffline;
    public boolean tutoringChannelOnline;
    public boolean tutoringPlaceStudentHome;
    public boolean tutoringPlaceTutorHome;
    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    public List<CourseSearch> courseSearches;

    public boolean[] preferredTime = new boolean[28];

    @Transient
    @Required
    @Min(value = 1, message = "validation.required")
    public long schoolTypeId;
    @Transient
    @Required @Min(value = 1, message = "validation.required")
    public long classLevelId;

    public StudentInformation(User user) {
        this.courseSearches = new ArrayList<CourseSearch>();
        this.user = user;
    }
}
