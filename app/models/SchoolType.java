/**
 * Copyright 2014 Technical University Munich, Software Engineering for Business Information Systems (sebis)
 * <p/>
 * Created by Duc Nguyen on 2014.06.04.
 */
package models;

        import play.db.jpa.Model;

        import javax.persistence.CascadeType;
        import javax.persistence.Entity;
        import javax.persistence.OneToMany;
        import java.util.List;

@Entity
public class SchoolType extends Model {
    public String name;

    @OneToMany(mappedBy = "schoolType", cascade = CascadeType.ALL)
    public List<StudentInformation> students;

    public SchoolType(String name) {
        this.name = name;
    }
}
