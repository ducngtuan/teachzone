package models;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import play.data.validation.Match;
import play.data.validation.MaxSize;
import play.data.validation.Min;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Entity
public class User extends Model {
    public String email;
    public String password;
    public String imageURL;
    @Required
    public String fullName;
    @Required
    public int genderCode;
    @Required @MaxSize(value = 200)
    public String description;
    @Required @Match(value = "\\d{5}", message = "validation.postalCode")
    public String postalCode;
    @Required
    public String address;
    @Required @Match(value = "[-+.() 0-9]+", message = "validation.phone")
    public String telephoneNumber;
    public String passwordSalt;
    @Required
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    public DateTime dateOfBirth;
    @Transient
    @Required @Min(value = 1, message = "validation.required")
    public long cityId;
    @ManyToOne
    public City city;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    public StudentInformation studentInformation;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    public TutorInformation tutorInformation;

    public User(String email, String fullName) {
        this.email = email;
        this.fullName = fullName;
    }

    public User(String email, String fullName, String password) {
        this.email = email;
        this.fullName = fullName;
        this.passwordSalt = getSalt();
        this.password = getSecurePassword(password, this.passwordSalt);
    }

    public void updatePassword(String newPassword) {
        this.passwordSalt = getSalt();
        this.password = getSecurePassword(newPassword, this.passwordSalt);
    }

    public int age() {
        LocalDate now = new LocalDate();
        Years age = Years.yearsBetween(dateOfBirth.toLocalDate(), now);
        return age.getYears();
    }

    public String gender() {
        if (genderCode == 1) return "Male";
        if (genderCode == 2) return "Female";
        return "Not specific";
    }

    /**
     * @return `true` if this user is a student
     */
    public boolean isStudent() {
        return studentInformation != null;
    }

    /**
     * @return `true` if this user is a tutor
     */
    public boolean isTutor() {
        return tutorInformation != null;
    }

    //generate salt
    private String getSalt() {
        String salt = "";
        try {
            salt = Long.toString(System.nanoTime()) + Long.toString(System.currentTimeMillis());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return salt;
    }

    private String getSecurePassword(String passwordToHash, String salt) {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(salt.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest(passwordToHash.getBytes());
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    public boolean authenticate(String password) {
        String hashedPassword = getSecurePassword(password, this.passwordSalt);
        return this.password.equals(hashedPassword);
    }
}
