import models.*;
import org.junit.Before;
import org.junit.Test;
import play.test.Fixtures;
import play.test.UnitTest;

public class BasicTest extends UnitTest {
    @Before
    public void setup() {
        Fixtures.deleteDatabase();
    }

    /**
     * Test the basic creating and retrieving an user.
     */
    @Test
    public void createAndRetrieveUser() {
        new User("foo@bar.com", "Foo Bar").save();
        User foo = User.find("byEmail", "foo@bar.com").first();
        assertNotNull(foo);
        assertEquals("Foo Bar", foo.fullName);
    }

    /**
     * Newly created user should have no student information,
     * and thus is not a student.
     */
    @Test
    public void newUserShouldHaveNoStudentInformation() {
        User foo = new User("foo@bar.com", "Foo Bar").save();
        assertNull(foo.studentInformation);
        assertFalse(foo.isStudent());
    }

    /**
     * Newly created user should have no tutor information,
     * and thus is not a tutor.
     */
    @Test
    public void newUserShouldHaveNoTutorInformation() {
        User foo = new User("foo@bar.com", "Foo Bar").save();
        assertNull(foo.tutorInformation);
        assertFalse(foo.isTutor());
    }

    /**
     * Test the relationship between StudentInformation, CourseSearch
     * and Subject.
     */
    @Test
    public void findStudentForSubject() {
        Category academic = new Category("Academic").save();
        Subject maths = new Subject(academic, "Maths").save();

        User user = new User("foo@bar.com", "Foo Bar").save();
        StudentInformation sti = new StudentInformation(user).save();
        assertEquals(0, sti.courseSearches.size());

        CourseSearch cs = new CourseSearch(sti, maths, null, 1).save();

        sti.refresh();
        assertEquals(1, sti.courseSearches.size());
        maths.refresh();
        assertEquals(1, maths.courseSearches.size());
        assertEquals(sti, maths.courseSearches.get(0).student);
        assertEquals(user, maths.courseSearches.get(0).student.user);

        Subject subject = Subject.find("byName", maths.name).first();
        assertEquals(user, subject.courseSearches.get(0).student.user);
    }

    /**
     * User authentication should return true only when the
     * input password is correct.
     */
    @Test
    public void checkUserAuthentication(){
        new User("mike@gmail.com", "Mike Tylor","12345").save();
        User user = User.find("byEmail", "mike@gmail.com").first();
        assertNotNull(user);
        assertTrue(user.authenticate("12345"));
        assertFalse(user.authenticate("abcd345"));
    }

    /**
     * User password should be able to be updated. The salt and
     * hashed password in the database should be changed.
     */
    @Test
    public void updatePasswordForUser(){
        new User("mike@gmail.com", "Mike Tylor","12345").save();
        User user = User.find("byEmail", "mike@gmail.com").first();
        assertNotNull(user);

        assertTrue(user.authenticate("12345"));
        assertFalse("12345".equals(user.password));
        String oldSalt = user.passwordSalt;
        String oldHashedPassword = user.password;

        user.updatePassword("abcd345");
        assertTrue(user.authenticate("abcd345"));
        assertFalse(oldSalt.equals(user.passwordSalt));
        assertFalse(oldHashedPassword.equals(user.password));
        assertFalse("abcd345".equals(user.password));
    }

    /**
     * Too users with the same password should have different salts,
     * and therefore different hashed password in the database.
     */
    @Test
    public void usersWithSamePasswordShouldHaveDifferentEncryptedPasswordInDB() {
        User u1 = new User("foo1@bar.com", "Foo Bar", "12345").save();
        User u2 = new User("foo2@bar.com", "Bar Foo", "12345").save();
        assertFalse(u1.passwordSalt.equals(u2.passwordSalt));
        assertFalse(u1.password.equals(u2.password));
    }
}
