# README #

This is a project for SEBA Master 2014. Teachzone is a Teaching Mediation Platform.

## How to begin? ##

* Check out repo
* Setting up [your preferred IDE](http://www.playframework.com/documentation/1.2.5/ide).
* Check out the [todo list on Producteev](https://www.producteev.com/workspace/p/5384e5c3126e95f360000000#). When you want to take over a task:
    * Assign yourself to the task.
    * Add the label `Doing` to it so others know that you are working on it
    * If done, mark the task as _completed_.
    * If need review or help, you can write note to card with some mention to some one else, or add a label to it.
    * A completed task can be reopened later.
* If there is something that you think that need to be done (function to be implemented, bug to be fixed, etc.), create a new task in Producteev.
* Bugs and issues can be put here or by creating a new task with a label `bug.

## Coding guideline ##

### General ###
* File encoding: UTF-8
* Tab-width: 4, use spaces for tabs.
* Before commit your changes, use auto-format function from IDE to re-format your code (`Ctrl` + `Alt` + `L` on IDEA)
* DON'T push garbage files (log, tmp, cache, etc.) or IDE specific files to repo. Ignore it if you need.
* Each commit should only for one fix or task
* As many commits as you like (many small commits are better than few big commits)

### Naming convention ###

For a model User, the following convention should be used:

* Model at `app/models/User.java` (without plural)
* Controller at `app/controllers/Users.java` **(with plural)**
* Views at `app/views/Users/xxxx.html`
* Route should begin with `/users/`, eg `/users/show`, `/users/index`

When defining a route:

* use `GET` method for route that showing content (eg. user profile page, user update form, etc.)
* use `POST` method for route that update or save content (eg. user save method used in update form),
  after updating content the controller should redirect the user.

### HTML Guidelines ###
* All Javascript files and codes should be included at the bottom of the `<body>` tag, just before the closing `</body>` tag (see http://stackoverflow.com/questions/11786915/javascript-on-the-bottom-of-the-page).

## Note on CSS stylesheet ##

All custom CSS settings should be put in file `pulic/css/main.css`. The file `public/css/custom_bootstrap.css` is compiled from `public/css/less/custom_bootstrap.less` and therefore should not be manually edited. To change setting of the Bootstrap theme, please edit file `public/css/less/custom_bootstrap.less` and re-compile it.

On IDEA you can install the plugin _Less Compiler_ or install a stand-alone compiler, for example: WinLess for Windows. Other information can be found on [Less Homepage](http://lesscss.org/). 

## Note on using IntelliJ IDEA ##

* First, please read guide on [how to setup IntelliJ](http://www.playframework.com/documentation/1.2.5/ide#intellij)
* The repo does not contain any IDE file, so you will have to run `play idealize` first.
* Open IDEA and open the project `*.ipr` file (normally `Teachzone.ipr`)
* You can run the app directly from IDEA (see the guide), or from command-line with `play run` or `play test` (`play test` is the better choice since it's both for running and testing the app)
* When open a java file for a first time, IDEA may as you to **Setup SDK**, just click on the Link and select the correct JDK (normally JDK 7). The JDK should be detected automatically by IDEA.